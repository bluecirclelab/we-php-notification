<?php
namespace blueCircleLab\WorshipExtremeNotification;

use Google\Cloud\PubSub\PubSubClient;

class PubSub {

  private static $client;

  /**
   * @return PubSubClient
   */
  public static function getClient(): PubSubClient {
    if (!self::$client) {
      $config = ['projectId' => self::getProjectId()];
      self::$client = new PubSubClient($config);
    }
    return self::$client;
  }

  private static function getProjectId(): string {
    return Template::isStaging() ? 'worshiptools-staging' : 'worship-extreme-datastore';
  }

}
