<?php
namespace blueCircleLab\WorshipExtremeNotification;

class Notification implements \JsonSerializable {

  private $type;
  private $user;
  private $email;
  private $push;
  private $url = 'https://planning.worshiptools.com/app/';

  public function __construct($type) {
    $this->type = $type;
  }

  public function jsonSerialize(): array {
    return get_object_vars($this);
  }

  public function setRecipient($id, $email) {
    $this->user = [
        'id' => $id,
        'email' => $email,
    ];
  }

  public function setUrl($url) {
    $this->url = $url;
  }

  public function setEmailPayload($subject, $body, $fromName = '', $replyTo = '') {
    $this->email = [
        'subject' => $subject,
        'payload' => $body,
        'fromName' => $fromName,
        'replyTo' => $replyTo,
    ];
  }

  public function setPushPayload($title, $body) {
    $this->push = [
        'title' => $title,
        'body' => $body,
    ];
  }

  /**
   * @param Notification[] $batch
   */
  public static function sendBatch(array $batch) {
    if (empty($batch)) { return; }
    PubSub::getClient()->topic('weNotification')->publishBatch(array_map(function($notification) {
      return [
          'data' => json_encode($notification)
      ];
    }, array_values($batch)));
  }

}
