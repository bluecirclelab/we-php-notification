<?php

namespace blueCircleLab\WorshipExtremeNotification;

use Composer\Script\Event;

class ComposerScripts {

  public static function downloadNotifications(Event $event) {
    $vendorDir = $event->getComposer()->getConfig()->get('vendor-dir');
    require $vendorDir . '/autoload.php';

    @mkdir(Template::getCacheDirectory());

    $collections = ['emails', 'notifications'];
    foreach ($collections as $collection) {
      $all = self::cmsRequest('/'.$collection, false);
      $localizations = array_map(function($l) {
        return $l['locale'];
      }, call_user_func_array('array_merge', array_column($all, 'localizations')));
      $localizations = array_unique($localizations);
      $localizations[] = 'en';

      foreach ($localizations as $localization) {
        self::cmsRequest('/'.$collection.'?'.http_build_query(['_locale' => $localization]));
      }
    }
  }

  private static function cmsRequest(string $url, bool $download = true) {
    error_log('Downloading '.$url);
    $client = new \GuzzleHttp\Client([
      'timeout' => 4,
      'base_uri' => Template::CMS_ROOT,
    ]);
    $response = $client->get($url);
    $response = $response->getBody()->getContents();
    if ($download) {
      file_put_contents(Template::getCacheKey($url), $response);
    }
    return json_decode($response, true);
  }

}
