<?php
namespace blueCircleLab\WorshipExtremeNotification;

use Twig\Environment;
use Twig\Loader\ArrayLoader;
use Twig\Loader\ChainLoader;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFunction;

class Template {

  const CMS_ROOT = 'https://cms.worship.tools';

  public static $DISABLE_CACHE = false;

  public static function getCacheDirectory(): string {
    return __DIR__.'/../cache';
  }

  public static function getCacheKey(string $url): string {
    $url = ltrim($url, '/');
    $key = preg_replace( '/[^a-z0-9]+/', '-', strtolower( $url ) ).'.json';
    return self::getCacheDirectory().DIRECTORY_SEPARATOR.$key;
  }

  public static function cmsRequest(string $url) {
    if (file_exists(self::getCacheKey($url)) && !self::$DISABLE_CACHE) {
      $response = file_get_contents(self::getCacheKey($url));
    } else {
      try {
        $client = new \GuzzleHttp\Client([
          'timeout' => 4,
          'base_uri' => self::CMS_ROOT,
        ]);
        $response = $client->get($url);
      } catch (\Exception $e) {
        throw new \Exception('Failed to get template from CMS.');
      }
      $response = $response->getBody()->getContents();
    }
    return json_decode($response);
  }

  private static function findInArray(array $array, string $key, string|int $value): ?object {
    if (!$array) { return null; }

    $id = array_search($value, array_column($array, $key));
    if ($id === false) { return null; }

    return $array[$id];
  }

  public static function getTerm($term, $language = 'en'): string {
    if ($language === 'en') {
      return $term;
    }

    // get English to get locale id
    $response = self::cmsRequest('/notifications?'.http_build_query([
        '_locale' => 'en',
    ]));
    $response = self::findInArray($response, 'term', $term);
    if (!$response) { return $term; }

    $locale = array_column($response->localizations, null, 'locale')[$language] ?? false;
    if (!$locale) { return $term; }

    $response = self::cmsRequest('/notifications?'.http_build_query([
      '_locale' => $language,
    ]));
    $response = self::findInArray($response, 'id', $locale->id);
    return $response->term;
  }

  public static function getTemplate($id, $data = [], $language = 'en') {
    $response = self::cmsRequest('/emails?'.http_build_query([
        '_locale' => $language,
    ]));
    $response = self::findInArray($response, 'template', $id);
    if (!$response) {
      return $language === 'en' ? null : self::getTemplate($id, $data);
    }

    $loader = new ChainLoader([
        new FilesystemLoader(__DIR__.'/templates'),
        new ArrayLoader([
            'none.twig' => '',
            'subject.twig' => $response->subject,
            'pushNotificationTitle.twig' => $response->pushNotificationTitle ?? '',
            'pushNotificationBody.twig' => $response->pushNotificationBody ?? '',
            'body.twig' => str_replace('<p',
                '<p style="margin:12px 0;"',
                (new \Parsedown())->setBreaksEnabled(true)->text($response->body)),
        ])
    ]);
    $twig = new Environment($loader);
    $twig->addFunction(new TwigFunction('_', [new Template(), 'getTerm']));
    $twig->addFunction(new TwigFunction('isStaging', [__CLASS__, 'isStaging']));
    $response->body = $twig->render($response->brand.'.twig', array_merge($data, ['language' => $language]));
    $response->subject = $twig->render('subject.twig', $data);
    $response->pushNotificationTitle = $twig->render('pushNotificationTitle.twig', $data);
    $response->pushNotificationBody = $twig->render('pushNotificationBody.twig', $data);
    return $response;
  }

  public static function isStaging():bool {
    return defined('STAGING_ENV') ? STAGING_ENV : false;
  }

}
